-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-06-2022 a las 03:58:43
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `yury`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_emergencia`
--

CREATE TABLE `contacto_emergencia` (
  `id_emergencia` int(11) NOT NULL,
  `rut` varchar(10) NOT NULL,
  `nombre_emergencia` text NOT NULL,
  `relacion_trabajador` text NOT NULL,
  `sexo` text NOT NULL,
  `contacto` int(9) NOT NULL,
  `parentesco` text NOT NULL,
  `carga_familiar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `contacto_emergencia`
--

INSERT INTO `contacto_emergencia` (`id_emergencia`, `rut`, `nombre_emergencia`, `relacion_trabajador`, `sexo`, `contacto`, `parentesco`, `carga_familiar`) VALUES
(1, '12377916-4', 'Alicia Cordoba', 'conyugue', 'femenino', 935435676, 'familiar', 'aplica'),
(2, '13349911-2', 'Pedro Aguilar', 'conyugue', 'masculino', 911277921, 'familiar', 'no aplica'),
(3, '20465639-5', 'Adrian Lopez', 'hijo', 'masculino', 979845954, 'familiar', 'aplica'),
(4, '10714982-3', 'Felipe Ceballos', 'padre', 'masculino', 965476387, 'familiar', 'aplica'),
(5, '12365758-4', 'Gabriela Alvarez', 'conyugue', 'femenino', 938568438, 'familiar', 'aplica'),
(6, '12698843-4', 'Carolina Perez', 'conyugue', 'femenino', 937755327, 'familiar', 'no aplica'),
(7, '12885932-7', 'Quimey Davalos', 'conyugue', 'femenino', 976583341, 'familiar', 'aplica'),
(8, '15017117-4', 'Antonia Ñuñez', 'hija', 'femenino', 977852347, 'familiar', 'no aplica'),
(9, '12275332-1', 'Carol Dance', 'conyugue', 'masculino', 912243201, 'familiar', 'aplica'),
(10, '12778923-4', 'Julian Troncoso', 'conyugue', 'masculino', 988554532, 'familiar', 'no aplica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_laborales`
--

CREATE TABLE `datos_laborales` (
  `id_contrato` int(11) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `contraseña` varchar(30) NOT NULL,
  `cargo` varchar(40) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `area` varchar(40) NOT NULL,
  `departamento` varchar(50) NOT NULL,
  `id_persona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `datos_laborales`
--

INSERT INTO `datos_laborales` (`id_contrato`, `usuario`, `contraseña`, `cargo`, `fecha_ingreso`, `area`, `departamento`, `id_persona`) VALUES
(1, 'juan1', '1234', 'mantenimiento', '2020-06-01', 'soporte y mantenimiento', '01', 1),
(2, 'ana2', '1234', 'secretaria', '2020-07-01', 'administracion', '02', 2),
(3, 'jhon3', '1234', 'mantenimiento', '2020-05-04', 'soporte y mantenimiento', '01', 3),
(4, 'carlos4', '1234', 'programador', '2020-08-03', 'desarrollo y programacion', '03', 4),
(5, 'martin5', '1234', 'programador', '2022-08-09', 'desarrollo y programacion', '03', 5),
(6, 'julio6', '1234', 'mantenimiento', '2020-09-08', 'soporte y mantenimiento', '01', 6),
(7, 'jose7', '1234', 'programador', '2019-05-14', 'desarrollo y programacion', '03', 7),
(8, 'diego8', '1234', 'administrador', '2018-04-02', 'administracion', '02', 8),
(9, 'carla9', '1234', 'mantenimiento', '2020-03-02', 'soporte y mantenimiento', '01', 9),
(10, 'leonor10', '1234', 'secretaria', '2018-06-11', 'administracion', '02', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id_persona` int(11) NOT NULL,
  `rut` varchar(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `sexo` text NOT NULL,
  `edad` int(2) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `direccion` varchar(40) NOT NULL,
  `telefono` int(9) NOT NULL,
  `visible` text NOT NULL,
  `id_emergencia` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id_persona`, `rut`, `nombre`, `sexo`, `edad`, `correo`, `direccion`, `telefono`, `visible`, `id_emergencia`) VALUES
(1, '12111001-9', 'Juan Carlos', 'Masculino', 27, 'juancarlos@gmail.com', 'avenida los robles 395', 999854319, 'si', 1),
(2, '12377419-8', 'Ana Maria', 'Femenino', 42, 'anamaria@gmail.com', 'pasaje el Carmen 112', 984218809, 'si', 2),
(3, '11775984-3', 'Jhon Lopez', 'Masculino', 39, 'jhonlopez@gmail.com', 'los notros 83', 974751159, 'si', 3),
(4, '14017419-8', 'Carlos Ceballos', 'Masculino', 24, 'carlosceballos@gmail.com', 'avenida los robles 100', 923454235, 'si', 4),
(5, '12327456-4', 'Martin Cardenas', 'Masculino', 25, 'martincardenas@gmail.com', 'francia 83', 935467865, 'si', 5),
(6, '12351234-4', 'Julio Cardenas', 'masculino', 30, 'juliocardenas@gmail.com', 'los álamos 33', 988743391, 'si', 6),
(7, '12110392-2', 'Jose Torres', 'masculino', 27, 'josemelina@gmail.com', 'los abedules 445', 944603255, 'si', 7),
(8, '10355132-8', 'Diego Ñuñez', 'masculino', 45, 'diegoñuñuez@gmail.com', 'pasaje Ñuble alto 355', 945538774, 'si', 8),
(9, '12699445-4', 'Carla Ramirez', 'femenino', 38, 'carlaramirez@gmail.com', 'avenida las mariposas 450', 966473176, 'si', 9),
(10, '12477376-4', 'Leonor Córdoba ', 'femenino', 29, 'leonorcordoba@gmail.com', 'avenida hidalgo 708', 925645345, 'si', 10);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contacto_emergencia`
--
ALTER TABLE `contacto_emergencia`
  ADD PRIMARY KEY (`id_emergencia`);

--
-- Indices de la tabla `datos_laborales`
--
ALTER TABLE `datos_laborales`
  ADD PRIMARY KEY (`id_contrato`),
  ADD KEY `id_persona` (`id_persona`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id_persona`),
  ADD KEY `id_emergencia` (`id_emergencia`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contacto_emergencia`
--
ALTER TABLE `contacto_emergencia`
  MODIFY `id_emergencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `datos_laborales`
--
ALTER TABLE `datos_laborales`
  MODIFY `id_contrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id_persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `datos_laborales`
--
ALTER TABLE `datos_laborales`
  ADD CONSTRAINT `datos_laborales_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`id_emergencia`) REFERENCES `contacto_emergencia` (`id_emergencia`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
